FROM alpine:3.15
ENV version master
COPY build/configs/* /etc/lighttpd/
# COPY build/startup.sh /opt/
WORKDIR /opt/cloudlog
RUN apk update && apk add --no-cache \
            git \
            lighttpd \
            php7 \
            php7-openssl \
            php7-curl \
            php7-mbstring \
            php7-xml \
            php7-ctype \
            php7-mysqli \
            php7-session \
            php7-cgi \
            php7-simplexml \
            php7-json \
            bash \
            curl && \
    git clone -b ${version} --depth 1 https://github.com/magicbug/Cloudlog.git /opt/cloudlog && \
    mkdir -p /run/lighttpd && chown -R root.lighttpd /run/lighttpd /opt/cloudlog && \
    echo "curl --silent http://localhost/index.php/lotw/load_users &>/dev/null" > /etc/periodic/weekly/cloudlog_load_users && \
    chmod +x /etc/periodic/weekly/cloudlog_load_users && \
    echo "curl --silent http://localhost/index.php/update/update_clublog_scp &>/dev/null" > /etc/periodic/weekly/cloudlog_update_clublog_scp && \
    chmod +x /etc/periodic/weekly/cloudlog_update_clublog_scp && \
    echo "/opt/cloudlog/update_cloudlog.sh &>/dev/null" > /etc/periodic/weekly/cloudlog_update && \
    chmod +x /etc/periodic/weekly/cloudlog_update && \
    chmod +x update_cloudlog.sh && \
    sed -i -e 's/root:www-data/root.lighttpd/' ./update_cloudlog.sh && \
    ./update_cloudlog.sh
## -e 's/BLEEDING_EDGE="true"/BLEEDING_EDGE="false"/'
LABEL version ${version}
# ENTRYPOINT [ "/opt/startup.sh" ] 
CMD ["lighttpd", "-f", "/etc/lighttpd/lighttpd.conf", "-D" ]